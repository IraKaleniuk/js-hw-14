"use strict";
function ChangeThem(){
    if(changeThem.classList.contains("virgin-america")) {
        logoHover.src = "./images/logo-hover-virgin-america.png";
        document.head.append(styleLink);
        changeThem.classList.remove("virgin-america")
        localStorage.setItem("them", "virgin-america");
    } else {
        styleLink.remove();
        logoHover.src = "./images/logo-hover.png";
        changeThem.classList.add("virgin-america");
        localStorage.setItem("them", "orange");
    }
}

const styleLink = document.createElement("link");
styleLink.rel = "stylesheet";
styleLink.href = "./css/style-virgin-america.css";

const changeThem = document.querySelector("#change-them");
const logoHover = document.querySelector(".logo-hover");

let them = localStorage.getItem("them");
if(them) {
    ChangeThem();
} else  {
    localStorage.setItem("them", "orange");
}

changeThem.addEventListener("click", ChangeThem);